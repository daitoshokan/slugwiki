# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

import logging


def index():
    """
    This is the main page of the wiki.  
    You will find the title of the requested page in request.args(0).
    If this is None, then just serve the latest revision of something titled "Main page" or something 
    like that. 
    """
    title = request.args(0) or 'main_page' #if request.args(0) is None, show the main wiki page
    #title = request.args(0) == "banana slug"
    aretheysame = request.args(0) == "banana_slug"
    form = None
    content = None
    display_title = title.title()
 
    page = db(db.pagetable.title == title).select().first()
    if page is None:
        db.pagetable.insert(title=title)
        page = db(db.pagetable.title==title).select().first()
        page_id = page.id
    else:
        page_id = page.id 
    
    ##if request.args(0) contains a page title...
    ##lookup that page,
    ## lookup the most recent revision, and display it
    rev = db(db.revision.reference_id == page_id).select(orderby=~db.revision.revTime).first() #EET VERKS!
    #db.revision.insert(page_id=page_id, body=form.vars.body)
    
    #r= db.revision(1)
    s= rev.body if rev is not None else ''  #SO DAHS THEES
    
    
    
    
    #if r is None:
        #db.revision.insert(reference_id=page_id, body=form.vars.body)
    


    # Are we editing?
    editing = request.vars.edit == 'true'
    # This is how you can use logging, very useful.
    logger.info("This is a request for page %r, with editing %r" %
                 (title, editing))
    if editing:
        # We are editing.  Gets the body s of the page.
        # Creates a form to edit the content s, with s as default.
        form = SQLFORM.factory(Field('body', 'text',
                                     label='Content',
                                     default=s
                                     ))
        # You can easily add extra buttons to forms.
        form.add_button('Cancel', URL('default', 'index', args=[title]))
        # Processes the form.
        if form.process().accepted:
            # Writes the new content.
            if rev is None:
                # First time: we need to insert it.
                db.revision.insert(reference_id = page_id, body = form.vars.body)
            else:
                # We update it.
                rev.update_record(body=form.vars.body)
            # We redirect here, so we get this page with GET rather than POST,
            # and we go out of edit mode.
            redirect(URL('default', 'index',args=[title]))
        content = form
    else:
        # We are just displaying the page
        content = s
    return dict(display_title=display_title, content=content, editing=editing, title=title,aretheysame=aretheysame)
##     Whenever you show a page, add a button that allows the user to edit it...  

##If the user clicks there, add for examlple a URL variable that says: edit=y
##..as in as in: URL('default', 'index', page_title, vars=dict(edit='y'))


# If a user clicks on that, then: 
#  
# request.args(0) will contain the page title
# request.vars.edit, which is normally None, will be 'y'
#In this case, you need to read the most recent revision of the page, offer it to the user for editing, and once done, store it as the new most recent revision for that page.
    
    
    
    # You have to serve to the user the most recent revision of the 
    # page with title equal to title.
    
    # Let's uppernice the title.  The last 'title()' below
    # is actually a Python function, if you are wondering.
    
#     rev = db(db.revision.reference_id == title).select(orderby=~db.revision.revTime).first()
#     if rev == None:
#         revision_id = db.revision.insert(page_id=page_id, body=form.vars.body)
    #it would seem rev is currently the empty string, which means in this case we create a new page
    
    # Here, I am faking it.  
    # Produce the content from real database data. 


def test():
    """This controller is here for testing purposes only.
    Feel free to leave it in, but don't make it part of your wiki.
    """
    title = "This is the wiki's test page"
    form = None
    content = None
    
    # Let's uppernice the title.  The last 'title()' below
    # is actually a Python function, if you are wondering.
    display_title = title.title()
    
    # Gets the body s of the page.
    #r = db.testpage(1)
    s = r.body if r is not None else ''
    # Are we editing?
    editing = request.vars.edit == 'true'
    # This is how you can use logging, very useful.
    logger.info("This is a request for page %r, with editing %r" %
                 (title, editing))
    if editing:
        # We are editing.  Gets the body s of the page.
        # Creates a form to edit the content s, with s as default.
        form = SQLFORM.factory(Field('body', 'text',
                                     label='Content',
                                     default=s
                                     ))
        # You can easily add extra buttons to forms.
        form.add_button('Cancel', URL('default', 'test'))
        # Processes the form.
        if form.process().accepted:
            # Writes the new content.
            if r is None:
                # First time: we need to insert it.
                db.testpage.insert(id=1, body=form.vars.body)
            else:
                # We update it.
                r.update_record(body=form.vars.body)
            # We redirect here, so we get this page with GET rather than POST,
            # and we go out of edit mode.
            redirect(URL('default', 'test'))
        content = form
    else:
        # We are just displaying the page
        content = s
    return dict(display_title=display_title, content=content, editing=editing)


def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/manage_users (requires membership in
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    """
    return dict(form=auth())


@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()


@auth.requires_login() 
def api():
    """
    this is example of API with access control
    WEB2PY provides Hypermedia API (Collection+JSON) Experimental
    """
    from gluon.contrib.hypermedia import Collection
    rules = {
        '<tablename>': {'GET':{},'POST':{},'PUT':{},'DELETE':{}},
        }
    return Collection(db).process(request,response,rules)
